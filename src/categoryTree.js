(function() {
    "user strict";

    function CategoryTree() {
        var CATEGORY_CONTAINER_ID = "category-tree";
        var _categoryTree = [];

        return {
            addCategory: addCategory,
            printTree: printTree
        };

        function _createCategory(title) {
            return {
                id: Math.floor(Date.now() / 1000),
                title: title,
                children: []
            }
        }

        function _clearTreeContainer() {
            var container = document.getElementById(CATEGORY_CONTAINER_ID);
            container.innerHTML = "";
        }

        function _printCategory(parentId, category) {
            var container = document.getElementById(parentId);
            var categoryElement = document.createElement("div");

            categoryElement.innerHTML = "ID: " + category.id + " title: " + category.title;
            categoryElement.id = category.id;
            categoryElement.style.paddingLeft = "15px";

            container.appendChild(categoryElement);
        }

        function _getCategoryById(tree, categoryId) {
            var category = null;

            for (var i = 0; i < tree.length; i++) {
                if (tree[i].id === categoryId) {
                    category = tree[i];
                    break;
                } else if (tree[i].children && tree[i].children.length > 0) {
                    category = _getCategoryById(tree[i].children, categoryId);
                }
            }

            return category;
        }

        function _printTreeRecursive(tree, parentId) {
            for (var i = 0; i < tree.length; i++) {
                _printCategory(parentId, tree[i]);

                if (tree[i].children.length > 0) {
                    _printTreeRecursive(tree[i].children, tree[i].id);
                }
            }
        }

        function _getDepth(tree) {
            var depth = 0;

            tree.forEach(function (category) {
                var tmpDepth = _getDepth(category.children)
                if (tmpDepth > depth) {
                    depth = tmpDepth
                }
            });

            return depth + 1;
        }

        function _getCategoryChildrenAtDepth(category, depth) {

        }

        function _printTreeIterative() {
            var treeDepth = _getDepth(_categoryTree);

            for (var i = 0; i < treeDepth; i++) {
                if (i === 0) {
                    for (var j = 0; j < _categoryTree.length; j++) {
                        _printCategory(CATEGORY_CONTAINER_ID, _categoryTree[j]);
                    }
                } else {
                    _getCategoryChildrenAtDepth(_categoryTree[j]);
                }
            }
        }

        function addCategory(title, parentCategoryId) {
            var parentcategory;

            if (parentCategoryId !== null && parentCategoryId !== undefined) {
                parentcategory = _getCategoryById(_categoryTree, parentCategoryId);
            }

            if (parentcategory) {
                parentcategory.children.push(_createCategory(title));
            } else {
                _categoryTree.push(_createCategory(title));
            }
        }

        function printTree() {
            _clearTreeContainer();

            _printTreeRecursive(_categoryTree, CATEGORY_CONTAINER_ID);
            // _printTreeIterative();
        }
    }

    var categoryTree = new CategoryTree();

    document.getElementById('add-category-btn').onclick = function() {
        var categoryTitle = document.getElementById("category-name").value;
        var parentCategoryId = parseInt(document.getElementById("parent-category-id").value);

        categoryTree.addCategory(categoryTitle, parentCategoryId);
        categoryTree.printTree();
    }
})();